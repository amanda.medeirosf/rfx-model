function [matlabbatch] = dcm_BMS_nofamily(F,  FXmodel,SaveName,dirbms)
% Ffree = Free Energy matrix, Nsubj * Nmodel  
% FXmodel = 'RFX' or 'FFX'
% FileName for the OUtput BMS file
% dirbms = where to save the BMS output file


%% load F
% load(Ffree); %F = Free Energy matrix, Nsubj * Nmodel

Nsubj=size(F,1);
Nm=size(F,2);

%% Define Models
subj=struct;

for k = 1:Nsubj
    
    model=struct;
    for i=1:Nm
        model(i).fname=[ '#' num2str(i) ]; %was commented
        model(i).F=F(k,i);
    end
    
    sess=struct;
    sess.model=model; clear model;
    subj(k).sess.model=sess.model;
 
end

Fmodelspace=[ dirbms '\ModelSpace.mat'];
save(Fmodelspace,'subj');



%% Build BMC Structure
matlabbatch{1}.spm.stats.bms.bms_dcm.dir = {dirbms};
matlabbatch{1}.spm.stats.bms.bms_dcm.sess_dcm = {};
matlabbatch{1}.spm.stats.bms.bms_dcm.model_sp = {Fmodelspace};
% matlabbatch{1}.spm.stats.bms.bms_dcm.load_f = {F,'F'}; %was commented
matlabbatch{1}.spm.stats.bms.bms_dcm.bma.bma_no = 0;
% matlabbatch{1}.spm.stats.bms.bms_dcm.bma.bma_yes.bma_all = 'famwin';
matlabbatch{1}.spm.stats.bms.bms_dcm.method = FXmodel; % 
matlabbatch{1}.spm.stats.bms.bms_dcm.verify_id = 0;


% Bayesian Model Comparison
spm('defaults','EEG');
%spm_jobman('serial',jobs,'',inputs{:});
spm_jobman('run',matlabbatch);

%% save
cd(dirbms);
Fsave=[ SaveName '.mat'];
movefile('BMS.mat', Fsave);
Figsave=[ SaveName '.fig'];
h=gcf;
saveas(h, Figsave, 'fig'); % uncommented for LM models (BMS per sample)