%03-07-2019%
clear;
subj = {'00','01','02','05','06','08','09','11','13','14','16','17','18','19','20','21','22','23','24','26'};
models = {'Tcd','Tlincd','Tdd','Tbinom_2','Tbinom_6','Tbinom_10','Tbinom_100'};
methods = {'_FreeEnergy_','_FreeEnergy_ASR_','_FreeEnergy_EMD_'};
addpath('FreeEnergyData');

LogBF_T = [];
LogBF_Pop_T = [];
FE = cell(2,3);

for met=1:length(methods)
    for m=1:length(models)
        for s=1:length(subj)
            a=load(strcat('suj',subj{s},methods{met},'S_E_T0_alls'));
            FreeEnergy_T0(s,:) = a.FreeEnergy;
            
            b=load(strcat('suj',subj{s},methods{met},'T_E_',models{m},'_alls'));
            FreeEnergy_T(s,:) = b.FreeEnergy;
            
            LogBF_T(s,:) = FreeEnergy_T(s,:) - FreeEnergy_T0(s,:);
            
        end
        
        
        LogBF_Pop_T(m,:) = sum(LogBF_T,1);
        
         LogBF_Pop_T(m,find(LogBF_Pop_T(m,:)<20))=0;
        
    end
    if met==1
        LogBF_Pop_T(:,36:40)=[];
    end
    
    
    
    FE{1,met}=LogBF_Pop_T;
    clear FreeEnergy_T0 FreeEnergy_T LogBF_T LogBF_Pop_T
    
end
    



for jj=1:3
    MaxValue(1,jj) = max(FE{1,jj}(:));
    MinValue(1,jj) = min(FE{1,jj}(:));
end


t = round(linspace(-50,300,8));
figure;

titlelabel = {'Offline','ASR','EMD'};
xx=1;

for jj=1:3
    subplot(1,3,xx); imagesc(FE{1,jj});
    caxis([min(MinValue(1,:)) max(MaxValue(1,:))]);
    xx=xx+1;
    ax = gca;
    ax.XTick = round(linspace(1,35,8));
    ax.XTickLabel = t;
    ax.YTick=1:length(models);
    ax.YTickLabel = {'NL cd','NL lincd','NL dd','L 2','L 6','L 10','L 100'};
    title(titlelabel{jj});
end

colormap(jet);





