% To compute RFX model according to Fran�oise (on paper)
% 02/07/2019

%%

clear;
subj = {'00','01','02','05','06','08','09','11','13','14','16','17','18','19','20','21','22','23','24','26'};
models = {'S_E_T0_alls','T_E_Tcd_alls','T_E_Tlincd_alls','T_E_Tdd_alls','T_E_Tbinom_2_alls','T_E_Tbinom_6_alls','T_E_Tbinom_10_alls','T_E_Tbinom_100_alls'};
methods = {'_FreeEnergy_','_FreeEnergy_ASR_','_FreeEnergy_EMD_'};
addpath(genpath('FreeEnergyData'));
addpath(genpath('BMS_RFX_results'));

% Ffree = Free Energy matrix, Nsubj * Nmodel;



for met=1:length(methods)
    for m=1:length(models)
        
        for s=1:length(subj)
            a=load(strcat('suj',subj{s},methods{met},models{m}));
            Ffree(s,m) = a.FreeEnergy(1,23); % time sample of the maximum Free-energy value
            
            
        end
        
    end
    
    [matlabbatch] = dcm_BMS_nofamily(Ffree,  'RFX',strcat('BMS_RFX_',methods{met}),'D:\Doutorado\Backup CRNL\Cluster\script\PaperAnalysis\BMS_RFX_results');
    clear Ffree;
end

